-----------------------------------------------------------------------------------------
--
-- main.lua
--
-----------------------------------------------------------------------------------------

local composer = require("composer")

function testButton()
	print("Button was Clicked.")
end

colorTable = {
	-- Devide RGB Values by 255
	abRed = { 255/255,0,0 },
	abGreen = { 0,255/255,0 },
	abBlue = { 0,0,255/255 },
	pnk = {200/255, 0, 150/255},
	prp = {145/255, 0, 249/255},
}

safeZone = {
	x = display.safeScreenOriginX,
	y = display.safeScreenOriginY,
	h = display.safeActualContentHeight,
	w = display.safeActualContentWidth,
}

menuButtonSize = {
	w = safeZone.w*0.15,
	h = safeZone.h*0.1,
}

composer.gotoScene("scripts.home")