local composer = require("composer")
local widget = require("widget")

local scene = composer.newScene()

local background
local intent

-- These will need adjusting
local menuWidth = display.contentCenterX
local menuHeight = display.displayContentHeight

-- Functions

function closeMenu()
	print("closeMenu was tapped")
end

-- Scene Functions

-- create()
function scene:create( event )
 
    local sceneGroup = self.view
    -- Code here runs when the scene is first created but has not yet appeared on screen
    if(event.params and event.params.from) then
		intent = event.params.from
	else
		intent = nil
	end
    -- Make the close button a close button...
    menuButton:addEventListener( "tap", closeMenu)
    -- Make a background for other content
    background = display.newRect(display.contentCenterX, safeZone.h, safeZone.w, safeZone.h*2) -- width, height, x, y
    background:setFillColor(unpack(colorTable.abGreen))
    -- Make a background for the menu
    local menuBack = display.newRect(sceneGroup, 0, safeZone.h, menuWidth*1.8, safeZone.h*2)
    menuBack:setFillColor(unpack(colorTable.prp)) 
    -- Deal with closing the menu
    local backgroundClose = display.newRect(sceneGroup, 0, safeZone.w, safeZone.h, ( safeZone.w - menuWidth ) , safeZone.h)
	backgroundClose:setFillColor(1,0,0,0)
	backgroundClose.isHitTestable = true
	background:toBack()
	backgroundClose:addEventListener( "touch", closeMenu )
end
 
 
-- show()
function scene:show( event )
 
    local sceneGroup = self.view
    local phase = event.phase
 
    if ( phase == "will" ) then
        -- Code here runs when the scene is still off screen (but is about to come on screen)
 
    elseif ( phase == "did" ) then
        -- Code here runs when the scene is entirely on screen
 
    end
end
 
 
-- hide()
function scene:hide( event )
 
    local sceneGroup = self.view
    local phase = event.phase
 
    if ( phase == "will" ) then
        -- Code here runs when the scene is on screen (but is about to go off screen)
 
    elseif ( phase == "did" ) then
        -- Code here runs immediately after the scene goes entirely off screen
 
    end
end
 
 
-- destroy()
function scene:destroy( event )
 
    local sceneGroup = self.view
    -- Code here runs prior to the removal of scene's view
 
end
 
 
-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------
 
return scene
