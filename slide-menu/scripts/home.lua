local composer = require("composer")

if (not menuButton) then
	menuButton = nil
end

local scene = composer.newScene()

function printDebug()
	print("Scene loaded.")
end

function openMenu()
	-- Kill off the Menu Button as we don't need it 
	menuButton:removeEventListener( "tap", openMenu )
	menuButton:removeSelf()
	menuButton = nil
	-- Turn menu button into a close button, using the same variable
	menuButton = display.newImageRect("icons/ic_close.png", menuButtonSize.w, menuButtonSize.h)
	menuButton.x = (safeZone.w * 0.05) + safeZone.x
 	menuButton.y = (safeZone.h * 0.05) + safeZone.y
 	menuButton:setFillColor(unpack(colorTable.pnk))
 	--Options to transition in
 	local options = 
	{
		isModal = true,
		effect = "fromLeft",
		time = 150,
		params = {
			from = composer.getSceneName( "current" ),
		}
	}
	composer.showOverlay("scripts.menu", options)
end
 
-- -----------------------------------------------------------------------------------
-- Scene event functions
-- -----------------------------------------------------------------------------------
 
-- create()
function scene:create( event )
 
    local sceneGroup = self.view
    print(colorTable.abBlue)
    display.setDefault("background", unpack(colorTable.abBlue))
 	if (not menuButton) then
 		menuButton = display.newImageRect("icons/ic_menu.png", menuButtonSize.w, menuButtonSize.h)
 		menuButton.x = (safeZone.w * 0.05) + safeZone.x
 		menuButton.y = (safeZone.h * 0.05) + safeZone.y
 		menuButton:addEventListener("tap", openMenu)
 		menuButton:setFillColor(unpack(colorTable.pnk))
 		printDebug()
 	end
end
 
 
-- show()
function scene:show( event )
 
    local sceneGroup = self.view
    local phase = event.phase
 
    if ( phase == "will" ) then
        -- Code here runs when the scene is still off screen (but is about to come on screen)
 
    elseif ( phase == "did" ) then
        -- Code here runs when the scene is entirely on screen
 
    end
end
 
 
-- hide()
function scene:hide( event )
 
    local sceneGroup = self.view
    local phase = event.phase
 
    if ( phase == "will" ) then
        -- Code here runs when the scene is on screen (but is about to go off screen)
 
    elseif ( phase == "did" ) then
        -- Code here runs immediately after the scene goes entirely off screen
 
    end
end
 
 
-- destroy()
function scene:destroy( event )
 
    local sceneGroup = self.view
    -- Code here runs prior to the removal of scene's view
 
end
 
 
-- -----------------------------------------------------------------------------------
-- Scene event function listeners
-- -----------------------------------------------------------------------------------
scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )
-- -----------------------------------------------------------------------------------
 
return scene